# 简答题
一 、请说出下列最终执行结果，并解释为什么
```javascript
var a = [];
for (var i=0;i<10;i++){
	a[i] = function(){
		console.log(i);
	}
}
a[6]()
```
答： 执行结果为10,  应为var 循环 每个 i 都是全局的 所以循环停止i为10 所以打印i为10。然而如果使用了let/const则会在当前代码块中产生一个块级上下文，这个i则是每次循环的私有变量

### 二、请说出下列最终的执行结果，并解释为什么
```javascript
var tmp = 123;
if(true){
	console.log(tmp)
	let tmp;
}
```
答： 会报错，因为if(){} 创建了一个作用域 这个作用域中用 let 定义了const变量 则不能在let定义之前访问 因为 let const 定义变量在定义之前都是暂时性锁区，即不能在定义之前访问

### 三、结合ES6新语法，用最简单的方式找出数组中的最小值
```javascript
var arr = [12,34,32,89,4];

const getArrayMinNumber = arg => Math.min(...arg)

getArrayMinNumber(arr);
```

### 四、请详细说明 var let const 三种声明变量的方式之间的具体差距
1. 语法：
```javascript
var a = 0;
let b = 0;
const c = 0;
```
2. const 定义的变量不可以修改（基本数据类型不可，引用类型可以改变内容），而且必须初始化
```javascript
const b = 1;
b = 2;//报错
const a; //报错 声明必须赋值
```
3. var 定义的变量可以修改，不初始化会输出undefined 不会报错
```javascript
var a = 1;
var a = 2;
var b;
console.log(b)//undefined
```
5. 4. let 是块级作用域，内部使用let 定义后，对外部无影响，不能在let定义之前访问变量
```javascript
let a = 1;
if(true){
	//console.log(a);//报错
	let a =3;
	console.log(a)//3
};
console.log(a);//1
```
### 五、请说出下列代码最终输出的结果，并解释为什么。
```javascript
var a = 10;
var obj = {
	a: 20,
	fn(){
		setTimeout(()=>{
			console.log(this.a);
		})
	}
}
obj.fn();
```
答： 20，箭头函数不改变this指向 ，所以this为obj this.a = obj.a == 20 , 如果不是箭头函数如果浏览器直接执行  那么就是 10 this为window 所以window.a  => 10 

### 六、简述`symbol`类型的用途
答： 
- ES6 引入了一种新的原始数据类型Symbol，表示独一无二的值。
- 接受一个参数为Symbl的描述
- 最大的用法是用来定义对象的唯一属性名
- for in 循环拿不到Symbol属性名 
- object.keys 也获取不到Symbol 属性
- JSON.stringify() 会忽略Symbol定义的属性
- Object.getOwnPropertySymbols();可以获取到所有Symbols属性
- Symbol.toStringTag是一个内置symbol，它通常作为对象的属性键使用，对应的属性值应该为字符串类型，这个字符串用来表示该对象的自定义类型，通常只有内置的Object.prototype.toString()方法采取读取这个标签并把它包含在自己的返回值里。

### 七、说说什么是浅拷贝，什么是深拷贝

答： 简单点来说，就是假设B复制了A，当修改A时，看B是否会发生变化，如果B也跟着变了，说明这是浅拷贝，如果B没变，那就是深拷贝。浅拷贝拷贝引用，深拷贝拷贝值

### 八、请简述TypeScript与javascript之间的关系

答：
1. TypeScript可以运行JavaScript所有代码和编码方式
2. 使用TypeScript中一些新的概念，可使JavaScript开发变得容易和快捷
3. TypeScript 加入一些新的概念(类) 使javascript实现一些复杂功能变得容易
4. javascript 可以直接同Typescript一起运行，编译器会将Typescript代码转换为javascript
5. Typescript中有静态类型,javascrip则没有
6. TypeScript中每一个数据必须规定其数据类型，JavaScript不要求
7. TypeScript为函数提供了缺省参数值。
8. TypeScript中有模块的概念，可以封装数据 类 函数 声明等信息在模块里面
9. TypeScript是javascript的一个超集

### 九、请谈谈你所认为的TypeScript 优缺点

答：
- 优点：加入了类型系统和加入新的类的概念，从代码上规避一些错误 
- 缺点：需要编译，浏览器无法直接运行

### 十、描述引用计数的工作原理和优缺点

答：
- 核心思想： 设置引用数，判断当前引用数是否为0

- 引用计数器

- 引用关系发生改变时修改引用数字

- #### 引用计数算法优点

  - 发现垃圾时立即回收
  - 最大限度减少程序暂停

- #### 引用计数算法缺点

  - 无法回收循环引用的对象
  - 时间开销大 资源开销大
```javascript
//循环引用的对象演示
function fn(){
    const obj1 = {};
    const obj2 = {};

    obj1.name = obj2;
    obj2.name = obj1;

    return 'obj1和obj2互相引用，如果不存在其他对象对它们的引用，obj1与obj2的引用 计数也仍然为1 互相引用的对象就是循环引用 不会被引用计数算法回收'
}
```

### 十一、描述标记整理算法的工作流程

答： 
- 标记整理可以看做是标记清除的增强

- 标记阶段的操作和标记清除一至

- 清除阶段会先执行整理，移动对象位置 - 让地址连续解觉碎片化

  - #### 优点

    - 减少碎片化空间

  - #### 缺点

    - 不会立即收回垃圾对象

### 十二、描述V8中新生代储存区垃圾回收的流程
答：
- 回收过程中采用复制算法+标记整理
- 新生代内存分为两个等大小空间
- 使用空间为From ，空闲空间为To
- 活动对象存储与From空间
- 标记整理后将活动对象拷贝子To空间
- From 与 To 交换空间完成释放

### 十三、描述增量标记算法在何时使用及工作原理

答： 使用时机：会穿插在程序的运行中执行。

工作原理： 对象存在直接可达和间接可达，将遍历对象标记，拆分成多个小步骤，先标记直接可达对象。间接可达的标记与程序执行交替执行，最终完成清除。














