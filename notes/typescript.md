# typescript

## 内容概述
- 强类型与弱类型
- 静态类型与动态类型
- javascript 自由类型系统的问题
- Flow 静态语言类型检查方案
- Typescript 语言规范与基本应用

### 强类型与弱类型
- #### 强类型和弱类型（类型安全）
    - 强类型要求必须是要求的某种类型
    - 弱类型不限制参数类型
    - 强类型有更强的类型约束，弱类型中几乎没有什么约束
    - 强类型语言不允许有任意的隐式类型转换，而弱类型则允许任意数据隐式类型转换
- #### 动态类型和静态类型（类型检查）
    - 静态
        - 一个变量申明式就明确类型，类型不可修改
    - 动态 
        - 运行时才可以明确类型，类型可随意修改

### JavaScript 类型系统特征
- 弱类型且动态类型
- 丢失了类型系统的可靠性
- 为什么不是强类型/静态类型
    - 早前javascirpt应用过于简单
    - 脚本语言=>没有编译环节=>静态类型需要编译时检查类型
- 大规模应用下，这种 ’优势‘ 就变为 短板

### 弱类型的问题
- 类型异常 运行时才能发现
- 类型不明确 函数功能会发生改变
- 对象添加属性 错误用法
- 代码量小的时候都可以约定，代码量大的情况下不能保证

```javascript
//弱类型的问题
const obj = {}



obj.foo();//调用不存在的方法

setTimeout(()=>{
    obj.add();
},10000)//模拟运行时


function add(a,b) {
    return a + b;
}

console.log(add(1,3));//4
console.log(add('1',3));//凉凉 13


const obj2 = {};

obj2[true] = 100;

console.log(obj['true']);//奇奇怪怪
```

###  强类型的优势
- 错误更早的暴露
- 代码更智能，编码更准确
- 重构更牢靠


### Flow
#### Flow概述
- Code Faster。因为通过使用flow.js，我们可以减少很多不必要的错误 --- flow.js会在你敲代码的过程中就帮你检查代码中的错误，这样就不用再运行的时候不停地去寻找错误了。、
- Code Smarter。对于像js这样的动态语言我们很难创建聪明的工具。而flow.js理解你的代码，使得你更聪明的写代码。
- Code Confidently。 对代码做出巨大的改变是非常恐怖的。flow.js可以帮助你更快地重构，这样你就不用担心很多可能出现的问题了。
- Code Bigger。 很多开发人员同时对一个项目开发是很困难的。而flow.js可以帮助你解决这个问题，因为即使是半年前写的代码，使用flow.js依然可以让你轻松看明白。 

### Flow快速入门
- //@flow 需要注解才会检查
- 需要安装flow-bin模块
- yarn flow 启动检查
- yarn flow stop停止检查程序

#### liunx 安装flow
1. 第一步是启用 Yarn 存储库。 首先使用以下curl 命令导入存储库的 GPG 密钥：
> curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
使用以下命令启用 Yarn APT 存储库：
> echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

2. 启用存储库后，更新包索引并安装 Yarn，其中包括：
> sudo apt update
> sudo apt install yarn
如果您的系统上尚未安装 Node.js，则上面的命令将安装它。 那些使用 nvm 的人可以跳过 Node.js 安装：
> sudo apt install --no-install-recommends yarn
3. 通过打印 Yarn 版本号验证安装
> yarn --version
在撰写本文时，最新版本的 Yarn 是版本 1.22.5。


### flow编译移除注解
- 运行环境时不需要注解 所以需要移除
- 官方提供了flow-remove-types
    - flow-remove-types 目录 目标目录
- @babel 
    - 安装  @babel/core @babel/cli @babel/preset-flow
    - 添加 .babelrc 文件指定 @babel/preset-flow编译
    - 命令 yarn babel src -d dist

### Flow 开发工具插件
- vscode  flow language support  中文路径会报错 报错只允许7位ASCII
- 需要关闭js的语法检测


### Flow 类型推断
- 自动推断类型提示相应错误

不加类型传入字符串 字符串不能做乘法运算 报错
![avatar](./类型推断.png)
正确
![avatar](./类型推断2.png)

### Flow 类型注解
```javascript
/**
 * 类型注解
 * 
 * @flow
 */


 function square(n:number){
    return n * n;
 }

 let num: number = square(100);

 num = 'string' //提示Cannot assign `'string'` to `num` because  string [1] is incompatible with  

 //返回值类型
 function foo (a:number,b:number):number{
    return a * b;
 }

 foo(1,2);
 foo('1',2);// Cannot call `foo` with `'1'` bound to `a` because  string [1] is incompatible with  number [2]


 //没有返回值需要标记为 void
 function a (): void{
    
 }
 
```
### 原始类型
```javascript
const a: string = 'string';
const b: number = 123;
const b1: number = NaN;
const b2: number = Infinity;
const b3: number = -Infinity;
const c: boolean = true;
const d: null = null;
const e: void = undefined;
const f: symbol = Symbol();
```


### Flow数组类型
```javascript
//一个全部由数字组成的数组
const array : Array<number> = [1,2,3,4,5,6]; 
const array1 : number[] = [1,2,3,4,5]

//可以单独指定 -> 元组

const array2: [number,string] = [1,''];

//对象
```

### Flow 对象类型
```javascript
const obj:{ foo: string, bar: number } = { foo: '', bar: 123 }
//可选
const obj1:{ foo?: string, bar: number } = { foo: '', bar: 123 }
//key value 限制为必须都为字符串
const obj3:{[string]: string} = { };
```


### Flow 函数类型
```javascript
function foo1(callback:(string,number) => void){
    callback('string',100);
}

foo1(function(str,n){
    //str => 字符串
    //n => 数字

    //不可以有返回值或者说返回值为undefiled
})
```

### Flow 特殊类型
```javascript
//字面量类型限制只能为某个值

const str :'foo' = 'foo';

//只能为可选值之一
const type : 'success'|'error'  = 'error'; 


const types: string|number = 1;
const types1: string|number = '1';

//别名
type strOrNum = string | number;

const strNum: strOrNum = 1;
const strNum1: strOrNum = '1';


// const gender :?number = null
// const gender :?number = null
const gender :?number = 1 // 这种写法 要么为制定类型要么为null或者undefiled
```


### Flow Mixed 与 Any
- 差距就是 一个强类型 一个弱类型

```javascript
function passMixed(value: mixed){
    value.substr(1);//报错
    value * value;//报错

    if(typeof value === 'string'){
        value.substr(1);
    }

    if(typeof value === 'number'){
        value * value;
    }
   
}   
//Mixed 标识可接受任意类型

passMixed(1)
passMixed(1)
passMixed(true)


//与上面代码相同
function passMixed1(value: any){
    value.substr(1);


    value * value;
}   

passMixed1(1)
passMixed1(1)
passMixed1(true)

//差距就是 一个强类型 一个弱类型
```


### Flow 类型小结



### 运行环境API




## TypeScript 概述
- javascript 类型系统 es6+ => javascript
- 任何一种javascript运行环境都支持
- 功能更强大，生态更加健全，更完善
- 缺点：
    - 语言多了很多概念（接口，泛型等）
    - 项目初期，Typescript会增加一些成本

### Typescript 快速上手
- 完全可以使用javascript 标准语法写代码

```javascript
const hello = (name: string) => {
    console.log(`hello ${name}`);
}
hello("Typescript");
```

### Typescript 配置文件
- tsc --init 创建ts配置文件
- target 设置编译后的javascript的标准
- module 编译规范  
- outDir 输入文件夹
- rootDir 编译根目录
- sourceMap 源代码映射
- strict 严格模式 所有成员都需要指定明确类型

注意：直接运行tsc 才会生效

### Typescript 标准库声明
- 标准库就是内置对象所对应的声明


###  TypeScript 中文错误消息
- tsc --locale zh-CN



### TypeScript 作用域问题
- 默认全局作用域 两个不同文件同时定义同一个变量报错 无法重新声明块范围变量“a”(const , let)
- 使用模块改变作用域


### TypeScript Object 类型
- 泛指非原始数据类型


### TypeScript 数组类型

### TypeScript 元组类型
- 固定长度固定类型的数组

### TypeScript 枚举类型
- 枚举会影响编译结果 枚举不会移除 会编译为一个双向键值对对象
- 枚举前加const 会移除枚举 使用地方变为值 并以注释显示键


### TypeScript 函数类型
- 形参和实参数列一致
- 可选参数选择？和默认参数 必须放在参数位置最后

```javascript
/**
 * 函数类型
*/
export {};

function func (a:number,b:string = '20',...rest: number[]): string{
    return '哈喽'
}

func(1,'2',213);
func(1,'2');
func(1);


const fun2:(a:number,b:number)=> string = function (a:number,b:number): string{
    return '';
}
```

### TypeScript 任意类型
- any 有安全问题 是动态类型不会检查

```javascript
/**
 * 任意类型
*/
export {};

function stringify(value: any){
    return JSON.stringify(value);
}


stringify(123)
stringify({})
stringify([]);


let str:any = "faslfj";

str = 123123;

str.boo();
```

### TypeScript 隐式类型推断


### TypeScript 类型断言
- 断言不是转换 只是编译过程中的一个概念
```javascript
 export {};
 
 const nums = [1,2,3,4,5,6,7];

 const res = nums.find(i=>i >5);

//  const square = res * res; //报错

const num1 = res as number;

const num2 = <number>res;//jsx不能使用
```

### TypeScript 接口 interfaces
- 约束对象结构 必须要求对象拥有接口所有成员
- 可选成员 ？，只读成员，动态成员
```javascript
interface Post{
    title: string
    content: string
    subtitle?: string, //可选成员
    readonly summary: string,//只读成员
}


function printPost (post: Post){
    console.log(post.title);
    console.log(post.content);
}

printPost({
    title:"张三",
    content: 'biubiu',
    summary: "",
});



//动态成员
interface Cache1 {
    [prop: string]: string
}


const cache: Cache1 = {};

cache.str = '';
```

### TypeScript 类的基本使用
- 用来描述一类具体对象的抽象成员
- es6以前JavaScript 通过函数 原型 模拟实现类,es6以后 增加了class TypeScript 增强了class相关语法
- public 公共成员
- private 私有成员
- protected 允许子类访问
```javascript
export {};

class Person {
    public name: string;
    age:number;
    private bilibili: string = "这是私有属性"
    //只允许子类中访问
    protected gender: boolean = false
    constructor(name: string, age: number){
        this.name = name;
        this.age = age;
    }

    sayHi (msg: string ):void{
        console.log(`我的名字是: ${this.name}, 年龄为 : ${this.age}, bilibili: ${this.bilibili}`)
        console.log(this.gender);
    }
}

//访问修饰符
const tom = new Person('Tom',80);

class Student extends Person {
    private constructor(name: string, age: number){
        super(name,age);
    }


    static create (name: string, age: number){
        return new Student(name,age);
    }
}

const jack = Student.create("jack",10);
```

###  TypeScript 类与接口
- interface:接口只声明成员方法，不做实现。
- class:类声明并实现方法。


### 抽象类
- 抽象类无法被实例化 只能被继承


### 泛型（Generics）
- 再定义函数接口和类的时候不去指定具体类型，调用时传递一个类型 作用是复用代码

```javascript
function createNumberArray(length: number, value: number) {
    let arr = Array<number>(length).fill(value);

    return arr;
}

function createStringArray(length: number, value: string) {
    let arr = Array<string>(length).fill(value);

    return arr;
}


function createArray<T>(length: number,value: T){
    let arr = Array<T>(length).fill(value);
    return arr;
}
```


### TypeScript 类型声明
- 一个成员在定义时没有声明明确类型  使用时可以在声明类型
- declare关键字声明
```javascript
import {camelCase} from "lodash";
import qs from "query-string"
// declare function  camelCase(input : string): string;

qs.parseUrl("/123")

const res  = camelCase("hello type");
```